<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'bd_riodatamine');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'algu3mbr');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-R xZ!>|^}&#jv_5P%l4<J=-yK.U@znbMF@)1<5j++`gwV}_dz25yJEAz;edce*?');
define('SECURE_AUTH_KEY',  'ML;w&%doCf03qMC#T`F;m+*mcj%shC@:d@&Z!bj-b+QW.HNiF.{Mq5&LY=Lo(m6(');
define('LOGGED_IN_KEY',    '6ydmU,;7_Em<h;g+5i]*A|`b&> OGvT$^MIA,Sy+/X4F|tT|`-t|*:F^rr@/v49C');
define('NONCE_KEY',        'j|*/;wKp~d<`i|CEXrR)$w?.||~7-jQhaDuGJw Je|K fb+MT)B.o10Eu|ICy01i');
define('AUTH_SALT',        '[{M-CHLIO[{nEH}O9ER]m):xKMFN@0-Uny,G-!I<N[eHU*}^VTKyl+@?YG7+q#M1');
define('SECURE_AUTH_SALT', '~jhp6.PA#oaJmweyvSaNM8I3Nd1@c|PC%du:Ah3b|ybc@MI7cv:sD_^IJ~wz46p/');
define('LOGGED_IN_SALT',   'fLm*6nD=}T3T$!mxz|_j#Z|IGz-_gz]x1rv_Xu3:@<IOTKc9K9xZ-,|*a+WDXUg%');
define('NONCE_SALT',       '-+#o1 0#!.LY y-W_r^ph|{R/mX2ERlEu`hw>e@eF-Rm+Xz1tCMamA(:EAsD-+~y');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
