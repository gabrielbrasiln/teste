<?php
/**
 * @package GeneratePress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php generate_article_schema( 'CreativeWork' ); ?>>
	<div class="inside-article">
		<?php do_action( 'generate_before_content'); ?>
		<div class="app-link">
			<div class="app-image">
				<?php the_post_thumbnail(); ?>
			</div>
			<a href="<?php echo esc_url( get_post_permalink() ); ?>">Ver Detalhes</a>

		</div>
		<div class="app-desc">
			<header class="entry-header">
				<?php the_title( sprintf( '<h2 class="entry-title" itemprop="headline"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
				<?php if ( 'post' == get_post_type() ) : ?>
				<div class="autor">
					Enviado por <span><?php the_field('autor'); ?></span>
				</div>
				<?php endif; ?>
			</header><!-- .entry-header -->
			<?php do_action( 'generate_after_entry_header'); ?>
			
			<?php if ( true == generate_show_excerpt() ) : ?>
				<div class="entry-summary" itemprop="text">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->
			<?php else : ?>
				<div class="entry-content" itemprop="text">
					<?php the_content(); ?>
					<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'generate' ),
						'after'  => '</div>',
					) );
					?>
				</div><!-- .entry-content -->
			<?php endif; ?>
			</div>
		<?php do_action( 'generate_after_entry_content'); ?>
		<?php do_action( 'generate_after_content'); ?>
	</div><!-- .inside-article -->
</article><!-- #post-## -->