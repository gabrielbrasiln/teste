<?php
/**
 * @package GeneratePress
 */
?>

<?php
	$the_cat = get_the_category();
	$category_name = $the_cat[0]->cat_name;
	$category_link = get_category_link( $the_cat[0]->cat_ID );
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php generate_article_schema( 'CreativeWork' ); ?>>
	<div class="inside-article">
		
		
		<header class="entry-header">
			<div class="entry-meta">
				<div class="autor">
					Enviado por <span><?php the_field('autor'); ?></span>
				</div>
				<div class="app-category">
					Categoria: <a href="<?php echo $category_link; ?>"><?php echo $category_name ?></a>
				</div>
			</div><!-- .entry-meta -->

			<?php if ( generate_show_title() ) : ?>
				<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>
			<?php endif; ?>		
			
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
				<div class="app-image">
					<?php the_post_thumbnail(); ?>
				</div>
			<?php endif; ?>
		</header><!-- .entry-header -->
		
		<?php do_action( 'generate_after_entry_header'); ?>
		<div class="entry-content" itemprop="text">
			<?php the_content(); ?>
			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'generate' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->
		<?php do_action( 'generate_after_entry_content'); ?>
		<?php do_action( 'generate_after_content'); ?>
	</div><!-- .inside-article -->
</article><!-- #post-## -->
